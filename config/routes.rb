Rails.application.routes.draw do
  root 'tickets#new'

  get   '/dashboard',             to: 'dashboard#index'
  get   '/staffs/sign_up',        to: 'signups#new'
  post  '/staffs/sign_up',        to: 'signups#create'
  post  '/',                      to: 'tickets#create'
  post  '/tickets/assign',        to: 'tickets#assign'
  post  '/reply',                 to: 'tickets#reply'
  post  '/question',              to: 'tickets#question'
  get   '/tickets/status/change', to: 'tickets#change_status'
  get   '/tickets/department/change', to: 'tickets#change_department'
  get   '/myticket',              to: 'tickets#myticket'
  get   '/activitylogs',          to: 'tickets#activitylogs'
  get   '/tickets/search',        to: 'dashboard#search'
  get   'departments/new',        to: 'departments#new'
  post  'departments/new',        to: 'departments#create'

  resources :tickets, only: [:show, :new, :create]
  devise_for :staffs, controllers: { sessions:      'staffs/sessions',
                                     passwords:     'staffs/passwords' }
end
