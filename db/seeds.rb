# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

department = Department.create(name: 'Unknown')

Staff.create(email: 'ekzosta@gmail.com', password: '123123123', admin: true, department: department)

Status.create([{name: 'Unassigned'}, {name: 'Open'}, {name: 'On hold'}, {name: 'Closed'}])


98.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  client = Client.create!(name: name, email: email)

  subject = "subject-#{n+100}"
  ticket = Ticket.create!(client_id: client.id,
                          subject: subject,
                          department: department)
  message = "My message #{n+100342}"
  Comment.create!(message: message, ticket: ticket, author: client)

  log = ticket.activity_logs.build(activity: 'Ticket was created')
  log.save
end
