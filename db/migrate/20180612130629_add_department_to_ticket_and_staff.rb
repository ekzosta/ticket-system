class AddDepartmentToTicketAndStaff < ActiveRecord::Migration[5.2]
  def change
    add_reference :tickets, :department, foreign_key: true
    add_reference :staffs, :department, foreign_key: true
  end
end
