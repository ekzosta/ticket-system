class AddNumberToTicket < ActiveRecord::Migration[5.2]
  def change
    add_column :tickets, :number, :string, nil: false
    add_index :tickets, :number, unique: true
  end
end
