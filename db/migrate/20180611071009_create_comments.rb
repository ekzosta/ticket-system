class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.text :message
      t.references :ticket, foreign_key: true
      t.references :author, polymorphic: true, index: true

      t.timestamps
    end
  end
end
