class AddSpecificUrlForClient < ActiveRecord::Migration[5.2]
  def change
    add_column :tickets, :link, :string
    add_index :tickets, :link, unique: true 
  end
end
