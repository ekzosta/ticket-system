require 'rails_helper'

RSpec.describe Ticket, type: :model do
  it { should validate_presence_of(:subject) }
  it { should validate_length_of(:subject).is_at_least(6) }
  it { should validate_length_of(:subject).is_at_most(48) }
  it { should belong_to :client }
  it { should belong_to :status }
  it { should belong_to :staff }
  it { should belong_to :department }
  it { should have_many :comments }
  it { should have_many(:activity_logs) }
end
