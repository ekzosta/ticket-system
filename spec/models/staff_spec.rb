require 'rails_helper'

RSpec.describe Staff, type: :model do
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:password) }
  it { should belong_to :department }
  it { should have_many(:tickets) }
end
