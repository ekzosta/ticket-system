require 'rails_helper'

RSpec.describe Department, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_length_of(:name).is_at_most(64) }
  it { should have_many(:tickets) }
  it { should have_many(:staffs) }
end
