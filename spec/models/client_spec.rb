require 'rails_helper'

RSpec.describe Client, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_length_of(:name).is_at_most(64) }
  it { should validate_length_of(:name).is_at_least(3) }
  it { should validate_presence_of(:email) }
  it { should validate_uniqueness_of(:email).case_insensitive }
  it { should validate_length_of(:email).is_at_most(64) }
  it { should validate_length_of(:email).is_at_least(5) }
  it { should allow_value("email@addresse.foo").for(:email) }
  it { should_not allow_value("foo").for(:email) }
  it { should have_many(:tickets) }
end
