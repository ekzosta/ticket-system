require 'rails_helper'

RSpec.describe ActivityLog, type: :model do
  it { should validate_presence_of(:activity) }
  it { should belong_to :ticket }
end
