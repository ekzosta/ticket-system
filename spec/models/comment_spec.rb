require 'rails_helper'

RSpec.describe Comment, type: :model do
  it { should validate_presence_of(:message) }
  it { should validate_length_of(:message).is_at_most(2048) }
  it { should belong_to :ticket }
  it { should belong_to :author }
end
