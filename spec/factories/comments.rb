FactoryBot.define do
  factory :comment do
    message "MyText"
    ticket
    author
  end
end
