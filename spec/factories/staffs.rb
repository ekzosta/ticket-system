FactoryBot.define do
  factory :staff do
    sequence :email do |n|
      "staff#{n}@example.com"
    end
    password 'password'
  end
end
