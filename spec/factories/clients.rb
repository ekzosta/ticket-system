FactoryBot.define do
  factory :client do
    name "MyString"
    sequence :email do |n|
      "person#{n}@example.com"
    end
  end
end
