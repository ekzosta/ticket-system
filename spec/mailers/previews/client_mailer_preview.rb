# Preview all emails at http://localhost:3000/rails/mailers/client_mailer
class ClientMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/client_mailer/ticket_created
  def ticket_created
    ClientMailerMailer.ticket_created
  end

  # Preview this email at http://localhost:3000/rails/mailers/client_mailer/ticket_replied
  def ticket_replied
    ClientMailerMailer.ticket_replied
  end

  # Preview this email at http://localhost:3000/rails/mailers/client_mailer/ticket_status
  def ticket_status
    ClientMailerMailer.ticket_status
  end

end
