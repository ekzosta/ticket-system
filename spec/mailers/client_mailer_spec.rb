require "rails_helper"

RSpec.describe ClientMailer, type: :mailer do
  ticket = FactoryBot.create(:ticket)
  describe "ticket_created" do
    let(:mail) { ClientMailer.ticket_created(ticket) }

    it "renders the headers" do
      expect(mail.subject).to eq("Ticket created")
      expect(mail.to).to eq([ticket.client.email])
      expect(mail.from).to eq(["ticket-system-onapp@herokuapp.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Hi")
    end
  end

  describe "ticket_replied" do
    let(:mail) { ClientMailer.ticket_replied(ticket) }

    it "renders the headers" do
      expect(mail.subject).to eq("You have replied")
      expect(mail.to).to eq([ticket.client.email])
      expect(mail.from).to eq(["ticket-system-onapp@herokuapp.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Hi")
    end
  end

  describe "ticket_status" do
    let(:mail) { ClientMailer.ticket_status(ticket) }

    it "renders the headers" do
      expect(mail.subject).to eq("Ticket changed status")
      expect(mail.to).to eq([ticket.client.email])
      expect(mail.from).to eq(["ticket-system-onapp@herokuapp.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Hi")
    end
  end

  describe "ticket_department" do
    let(:mail) { ClientMailer.ticket_department(ticket) }

    it "renders the headers" do
      expect(mail.subject).to eq("Ticket changed department")
      expect(mail.to).to eq([ticket.client.email])
      expect(mail.from).to eq(["ticket-system-onapp@herokuapp.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Hi")
    end
  end

end
