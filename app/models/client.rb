class Client < ApplicationRecord
  has_many :tickets
  has_many :comments, as: :author

  accepts_nested_attributes_for :tickets

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+[a-z\d]+\.[a-z]+\z/i
  validates :name, presence: true, length: { minimum: 3,
                                             maximum: 64 }
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX },
                                    length: { minimum:5,
                                              maximum: 64 },
                                    uniqueness: { case_sensitive: false }
end
