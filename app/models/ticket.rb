class Ticket < ApplicationRecord
  belongs_to :client
  belongs_to :status
  belongs_to :staff, optional: true
  belongs_to :department
  has_many :comments
  has_many :activity_logs

  accepts_nested_attributes_for :comments

  scope :unassigneds, -> (department) { where(status: 1,
                                              department: department)
                                        .includes(:status) }
  scope :opens, -> (current_staff) { where(status: 2,
                                           staff: current_staff)
                                     .order(:updated_at)
                                     .includes(:status) }
  scope :onholds, -> (current_staff) { where(status: 3,
                                             staff: current_staff)
                                       .order(:updated_at)
                                       .includes(:status) }
  scope :closeds, -> (current_staff) { where(status: 4,
                                             staff: current_staff)
                                       .order(:updated_at)
                                       .includes(:status) }
  scope :search, -> (current_staff,
                      search,
                      department) { where(staff: current_staff)
                                    .or(where(status: 1,
                                              department: department))
                                    .where("lower(number) LIKE ? OR lower(subject) LIKE ?",
                                           "%#{search.downcase}%", "%#{search.downcase}%")
                                    .order(:updated_at)
                                    .includes(:status) }

  before_create :generate_link
  before_create :inc_number

  validates :subject, presence: true, length: { minimum: 6,
                                                maximum: 48 }

  private
    def generate_link
      self.link = SecureRandom.urlsafe_base64
    end

    def inc_number
      number = Ticket.last ? Ticket.last.number : 'AAA-00-AAA-00-AAA'
      def increment(src)
        if src.length == 2
          src = (src.to_i(16).next % 256).to_s(16).upcase.rjust(2,'0')
        else
          src = src.next
          src = 'AAA' if src == 'AAAA'
        end
        src
      end

      s_t_1 = number[0..2]
      s_h_1 = number[4..5]
      s_t_2 = number[7..9]
      s_h_2 = number[11..12]
      s_t_3 = number[14..16]

      t_s_t_1 = s_t_1
      t_s_h_1 = s_h_1
      t_s_t_2 = s_t_2
      t_s_h_2 = s_h_2
      t_s_t_3 = s_t_3

      s_t_3 = increment(s_t_3)
      s_h_2 = increment(s_h_2) if s_t_3 < t_s_t_3
      s_t_2 = increment(s_t_2) if s_h_2 < t_s_h_2
      s_h_1 = increment(s_h_1) if s_t_2 < t_s_t_2
      s_t_1 = increment(s_t_1) if s_h_1 < t_s_h_1

      self.number = s_t_1 + '-' + s_h_1 + '-' + s_t_2 + '-' + s_h_2 + '-' + s_t_3
    end
end
