class Staff < ApplicationRecord
  belongs_to :department
  has_many :tickets
  has_many :comments, as: :author
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

end
