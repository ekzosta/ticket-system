class Status < ApplicationRecord
  has_many :tickets

  validates :name, presence: true, length: { minimum: 3,
                                             maximum: 32 }
end
