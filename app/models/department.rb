class Department < ApplicationRecord
  has_many :tickets
  has_many :staffs

  validates :name, presence: true, length: { maximum: 64 }
end
