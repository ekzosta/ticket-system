class Comment < ApplicationRecord
  belongs_to :ticket
  belongs_to :author, polymorphic: true

  validates :message, presence: true, length: { maximum: 2048 }
end
