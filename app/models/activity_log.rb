class ActivityLog < ApplicationRecord
  belongs_to :ticket
  
  validates :activity, presence: true
end
