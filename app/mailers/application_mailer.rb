class ApplicationMailer < ActionMailer::Base
  default from: 'ticket-system-onapp@herokuapp.com'
  layout 'mailer'
end
