class ClientMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.client_mailer.ticket_created.subject
  #
  def ticket_created(ticket)
    @ticket = ticket
    mail to: ticket.client.email, subject: "Ticket created"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.client_mailer.ticket_replied.subject
  #
  def ticket_replied(ticket)
    @ticket = ticket
    mail to: ticket.client.email, subject: "You have replied"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.client_mailer.ticket_status.subject
  #
  def ticket_status(ticket)
    @ticket = ticket
    mail to: ticket.client.email, subject: "Ticket changed status"
  end

  def ticket_department(ticket)
    @ticket = ticket
    mail to: ticket.client.email, subject: "Ticket changed department"
  end
end
