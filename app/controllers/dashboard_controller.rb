class DashboardController < ApplicationController
  include Kaminari::Helpers::HelperMethods

  before_action :signed_in?, only: [:index, :search]

  def index
    @unassigneds = Ticket.unassigneds(current_staff.department).page(params[:unassigneds_page])
    @opens = Ticket.opens(current_staff).page(params[:opens_page])
    @onholds = Ticket.onholds(current_staff).page(params[:onholds_page])
    @closeds = Ticket.closeds(current_staff).page(params[:cloaseds_page])
  end

  def search
    @searcheds = Ticket.search(current_staff, params[:search], current_staff.department)
                 .page(params[:page])

    # tickets_rendered = '<div class="tickets">' + render_to_string(@searcheds) + '</div>'
    # pagination_rendered = paginate @searcheds
    respond_to do |format|
      format.js { render layout: false }
    end
  end
end

