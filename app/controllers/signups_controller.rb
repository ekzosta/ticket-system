class SignupsController < ApplicationController
  before_action :is_admin?, only: [:new, :create]
  
  def new
    @staff = Staff.new
    @department = Department.new
  end

  def create
    @department = Department.find(department_params)
    @staff = Staff.new(staff_params)
    @staff.department = @department
    if @staff.save
      flash[:notice] = 'Staff created'
      redirect_to dashboard_path
    else
      render 'new'
    end
  end

  private
    def staff_params
      params.require(:staff).permit(:email, :password)
    end

    def department_params
      params.require(:department).permit(:id)[:id]
    end
end
