class ApplicationController < ActionController::Base
  
  private
    def signed_in?
      if current_staff.nil?
        flash[:alert] = 'You must be signed!'
        redirect_to new_staff_session_path
      else
        true
      end
    end
    
    def is_admin?
      if current_staff.nil?
        redirect_to new_staff_session_path
      else
        if current_staff.admin?
          true
        else
          redirect_to root_path
        end
      end
    end
end
