class TicketsController < ApplicationController
  before_action :signed_in?, only: [:show, :assign, :change_status, :change_department]
  before_action :get_ticket, only: [:show, :assign, :change_status, :change_department, :reply]
  before_action :get_ticket_by_link, only: [:myticket, :question]
  before_action :get_client, only: [:show, :myticket, :question]
  before_action :get_comments, only: [:show, :myticket]

  def show
    @status = Status.new
    @comment = Comment.new
    @department = Department.new
  end

  def myticket
    @comment = Comment.new
  end

  def new
    @ticket = Ticket.new
    @client = Client.new
    ticket = @client.tickets.build
    comment = ticket.comments.build
    @comment = Comment.new
    @department = Department.new
  end

  def create
    @department = Department.find(department_params)
    @client = Client.find_by(email: client_params[:email].downcase)
    if @client
      @client.tickets.build(ticket_params)
    else
      @client = Client.new(client_params)
    end
    @client.email.downcase!
    @client.tickets.last.department = @department
    @client.tickets.last.comments[0].author = @client

    if @client.save
      flash[:notice] = 'Ticket created'
      @client.tickets.last.activity_logs.create(activity: 'Ticket was created')
      ClientMailer.ticket_created(@client.tickets[0]).deliver
      redirect_to root_path
    else
      render 'new'
    end
  end

  def assign
    @ticket.update(staff: current_staff, status_id: 2)
    @ticket.activity_logs.create(activity: 'Ticket was assigned by ' + @ticket.staff.email)
    redirect_to @ticket
    ClientMailer.ticket_status(@ticket).deliver
  end

  def change_status
    status_before = @ticket.status
    @ticket.staff_id = nil if params[:status_id] == 1
    @ticket.update(status_id: params[:status_id])
    @ticket.activity_logs.create(activity: 'Ticket changed status from ' + status_before.name + ' to status ' + @ticket.status.name)
    ClientMailer.ticket_status(@ticket).deliver
  end

  def change_department
    department_before = @ticket.department
    @ticket.update(department_id: params[:department_id])
    @ticket.activity_logs.create(activity: 'Ticket moved from ' + department_before.name + ' to ' + @ticket.department.name + ' department')
    ClientMailer.ticket_department(@ticket).deliver
  end

  def reply
    @comment = Comment.new(reply_params)
    @comment.update(author: current_staff, ticket: @ticket)
    flash[:notice] = 'Replyed!'
    @ticket.activity_logs.create(activity: @ticket.staff.email + ' replied')
    ClientMailer.ticket_replied(@ticket).deliver
    redirect_to @ticket
  end

  def question
    @comment = Comment.new(reply_params)
    @comment.update(author: @client, ticket: @ticket)
    flash[:notice] = 'Replyed!'
    @ticket.activity_logs.create(activity: @ticket.client.name + ' replied')
    redirect_to myticket_path(link: @ticket.link)
  end

  def activitylogs
    @logs = ActivityLog.where(ticket_id: params[:id]).page(params[:page])
  end

  private
    def client_params
      params.require(:client)
            .permit(:name, :email,
                     tickets_attributes: [:subject,
                                           comments_attributes: [:message]
                                         ])
    end

    def comment_params
      params.require(:ticket).require(:comment).permit(:message)
    end

    def reply_params
      params.require(:comment).permit(:message)
    end

    def ticket_params
      params.require(:client)
            .require(:tickets_attributes)
            .require('0')
            .permit(:subject,
                      comments_attributes: [:message]
                    )
    end

    def department_params
      params.require(:client).require(:department).permit(:id)[:id]
    end

    def get_ticket
      @ticket = Ticket.find(params[:id])
    end

    def get_ticket_by_link
      @ticket = Ticket.find_by(link: params[:link])
    end

    def get_client
      @client = @ticket.client
    end

    def get_comments
      @comments = Comment.where(ticket: @ticket).includes(:author)
                                                .order(:created_at)
                                                .page(params[:page])
    end
end
