class DepartmentsController < ApplicationController
  before_action :is_admin?, only: [:new, :create]
  
  def new
    @department = Department.new
  end

  def create
    @department = Department.new(department_params)
    if @department.save
      flash[:notice] = 'Department created'
      redirect_to dashboard_path
    else
      render 'new'
    end
  end

  private
    def department_params
      params.require(:department).permit(:name)
    end
end
