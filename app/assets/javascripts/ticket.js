$(function() {
  $("select#status_id").on("change", function() {
    $.ajax({
      url:  "/tickets/status/change",
      type: "GET",
      data: { id: $("select#status_id").data("selected"), status_id: $("select#status_id").val()}
    }).done(function (data) {
      window.location.href = $("select#status_id").data("path");
    });
  });
  $("select#department_id").on("change", function() {
    $.ajax({
      url:  "/tickets/department/change",
      type: "GET",
      data: { id: $("select#department_id").data("ticket"), department_id:  $("select#department_id").val()}
    }).done(function (data) {
      window.location.href = $("select#department_id").data("path");
    });
  });
});
