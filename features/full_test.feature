#@selenium 
#@javascript
Feature: Allow Cucumber to rescue exceptions
  Scenario: Find or create client and create ticket
    Given I go to create ticket page
    Then I fill in fields and create ticket
    Then I see success notice
    Then I logout
    Then I have created admin
    Then I go to login page and feel form by admin
    Then I see dashboard page
    Then I see new staff button and go
    Then I create new staff
    Then I create new department
    Then I logout
    Then I go to login page and feel form by staff
    Then I see dashboard page
    Then I select ticket
    Then I get ticket
    Then I wrote reply
    Then I change status to On hold
    Then Client wrote reply
    Then I logout
    Then I go to login page and feel form by staff
    Then I wrote reply
    Then I change status to Closed
