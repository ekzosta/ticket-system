Given("I go to create ticket page") do
  DatabaseCleaner.strategy = :truncation
  DatabaseCleaner.clean
  visit root_path
end

Then("I fill in fields and create ticket") do
  @status = FactoryBot.create(:status, name: 'Unassigned')
  FactoryBot.create(:status, name: 'Open')
  FactoryBot.create(:status, name: 'On hold')
  FactoryBot.create(:status, name: 'Closed')
  @department = FactoryBot.create(:department)

  @ticket_subject = 'Subject of ticket'
  visit root_path
  within('#new_client') do
    fill_in 'ticket_name', with: 'pasdddsword'
    fill_in 'ticket_email', with: 'someemail11@mail.mail'
    fill_in 'ticket_subject', with: @ticket_subject
    fill_in 'ticket_message', with: 'Some message'
    click_on('Create new ticket')
  end
  @ticket = Ticket.first
end

Then("I see success notice") do
  expect(page).to have_css('.alert-success')
end

Then("I have created admin") do
  @password = 'pasdddsword'
  @admin = FactoryBot.create(:staff, email: 'email@mai.com',
                                      password: @password,
                                      admin: true,
                                      department: @department)
end

Then("I go to login page and feel form by admin") do
  visit new_staff_session_path
  within('#new_staff') do
    fill_in('Email', options = { id: 'staff_email', with: @admin.email } )
    fill_in('Password', options = { id: 'staff_password', with: @password } )
    click_on(class: 'login-button')
  end
end

Then("I see dashboard page") do
  expect(page).to have_current_path(dashboard_path)
end

Then("I see new staff button and go") do
  find('#create_new_staff').click
  expect(page).to have_current_path(staffs_sign_up_path)
end

Then("I create new staff") do
  @email = 'staff@mail.com'
  within('#new_staff') do
    fill_in('Email', options = { id: 'staff_email', with: @email } )
    fill_in('Password', options = { id: 'staff_password', with: @password } )
    click_on('Create new staff')
  end
end

Then("I create new department") do
  find('#create_new_department').click
  expect(page).to have_current_path(departments_new_path)
  within('#new_department') do
    fill_in 'department_name', with: 'This is new department'
    click_on('Create new')
  end
end

Then("I logout") do
  visit('/staffs/sign_out')
end

Then("I go to login page and feel form by staff") do
  visit new_staff_session_path
  within('#new_staff') do
    fill_in('Email', options = { id: 'staff_email', with: @email } )
    fill_in('Password', options = { id: 'staff_password', with: @password } )
    click_on(class: 'login-button')
  end
end

Then("I select ticket") do
  find_link(@ticket_subject).click
end

Then("I get ticket") do
  find_link('Get ticket!').click
  visit dashboard_path
  find_link('Open').click
  find_link(@ticket_subject).click
end

Then("I wrote reply") do
  visit ticket_path(@ticket.id)
  fill_in 'comment_message', with: 'Omg this is some reply message'
  click_on('Send!')
  expect(page).to have_css('.alert-success')
end


Then("I change status to On hold") do
  select('On hold', from: 'status_id').select_option
  visit dashboard_path
  find_link('On hold').click
  find_link(@ticket_subject).click
end

Then("Client wrote reply") do
  visit myticket_path(link: @ticket.link)
  fill_in 'comment_message', with: 'This is only question...'
  click_on('Send!')
  expect(page).to have_css('.alert-success')
end

Then("I change status to Closed") do
  select('Closed', from: 'status_id').select_option
  visit dashboard_path
  find_link('Closed').click
  find_link(@ticket_subject).click
end
